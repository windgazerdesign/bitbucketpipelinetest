# Bitbucket Pipeline Test

This is a repository I whipped up to do some testing with Bitbucket Pipelines. I just want
to quickly determine how to work with them.

Basically this is just a simple grunt/npm javascript repository with the Mocha/Chai as a
testing framework. Since something will have to tell the CI it's success or failure.
